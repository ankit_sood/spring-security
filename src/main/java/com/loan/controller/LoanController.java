package com.loan.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loan")
public class LoanController {

	@GetMapping
	public ResponseEntity<String> getLoanDetails(){
		return new ResponseEntity<String>("Loan Get Endpoint", HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<String> addLoanDetails(){
		return new ResponseEntity<String>("Loan Post Endpoint", HttpStatus.CREATED);
	}
}
