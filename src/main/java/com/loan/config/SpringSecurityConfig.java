package com.loan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import com.loan.error.CustomAccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
    private CustomAccessDeniedHandler accessDeniedHandler;
	
	@Autowired
	private AuthenticationEntryPoint restAuthenticationEntryPoint;
	
	@Autowired
	private SimpleUrlAuthenticationSuccessHandler loanSavedRequestAwareAuthenticationSuccessHandler;
	
	private SimpleUrlAuthenticationFailureHandler laonUrlAuthenticationFailureHandler = new SimpleUrlAuthenticationFailureHandler();
	
	//Create 2 users for demo
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("user").password(encoder().encode("userPass")).roles("USER")
			.and()
			.withUser("admin").password(encoder().encode("password")).roles("USER", "ADMIN");
	}

	// Secure the endpoins with HTTP Basic authentication
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.authorizeRequests()
			.and()
			.exceptionHandling()
			.accessDeniedHandler(accessDeniedHandler)
			.authenticationEntryPoint(restAuthenticationEntryPoint)
			.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.GET,"/loan/**").authenticated()
			.antMatchers(HttpMethod.POST,"/loan/**").hasRole("ADMIN")
			.antMatchers("/h2console/**").permitAll()
			.and()
			.formLogin()
			.successHandler(loanSavedRequestAwareAuthenticationSuccessHandler)
			.failureHandler(laonUrlAuthenticationFailureHandler)
			.and()
			.httpBasic()
			.and()
			.logout();
		http.headers().frameOptions().disable();
	}
	
	@Bean
	public PasswordEncoder  encoder() {
	    return new BCryptPasswordEncoder();
	}

}
