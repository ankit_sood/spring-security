**Welcome to Loan Service**

##Introduction###
This is project which uses loan-service in order to demonstrate, how to protect an endpoint.It uses http basic authorization and authentication and It uses in memory database 
to save details related to the username and password. It doesn't save the password in plain text and uses BCryptPasswordEncoder.

---

##Explanation##

All the security related configuration are done in class called SpringSecurityConfig which extends WebSecurityConfigurerAdapter & overrides following methods:
- configure(AuthenticationManagerBuilder auth) : contains information related to authentication and authorizations.
- configure(HttpSecurity http) : contains information that defines which end-points are protected, what are the entry points and how the success/failure will be handled. 

###AuthenticationEntryPoint or Simply Entry Point:###
In a normal web application authentication will be triggered  if an un-autenticated user tries to access a secured page and will be redirected to Login page for credentials.
However, this behaviour is not right for a REST api. It should consider every request as a new request and should be able to authenticate all the requests. Every request which
doesn't have the credentials should simply fail with 401 un-authorised.

Spring security handles this automatic triggering of the authentication process with the help of Entry point.This is a required part of configuration and can be injected via 
authenticationEntryPoint.Keeping in mind that this functionality doesn’t make sense in the context of the REST Service, we define the new custom entry point to simply return 
401 when triggered.

###The Login Form for REST:###
- There are multiple ways to do Authentication for a REST API. One of the defaults Spring Security provides is Form Login – which uses an authentication processing 
filter – org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.

- The formLogin element will create this filter and also provides additional methods successHandler and failureHandler to set our custom authentication success and 
failure handlers respectively.

###Authentication should Return 200 Instead of 301(loanSavedRequestAwareAuthenticationSuccessHandler):###
By default, form login will answer a successful authentication request with a 301 MOVED PERMANENTLY status code; this makes sense in the context of an actual login form which needs to 
redirect after login.However, for a RESTful web service, the desired response for a successful authentication should be 200 OK.

We do this by injecting a custom authentication success handler in the form login filter, to replace the default one. The new handler implements the exact same login as the default 
org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler with one notable difference – it removes the redirect logic.


###Failed Authentication should return 401 instead of 302(laonUrlAuthenticationFailureHandler):###
Similarly – we configured the authentication failure handler – the same way we did with the success handler.
Luckily – in this case, we don’t need to actually define a new class for this handler – the standard implementation – SimpleUrlAuthenticationFailureHandler – does just fine.
---

###Testing###
To test this project please use postman with the following url and bodies.
1. GET - http://localhost:9090/loan
2. POST - http://localhost:9090/loan 
Both these requests have Basic Auth as part of HTTP headers.
	where Username: Admin and Password: password.
All these things are part of SpringSecurityConfig.
---